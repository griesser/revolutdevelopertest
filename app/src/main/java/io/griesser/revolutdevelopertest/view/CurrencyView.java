package io.griesser.revolutdevelopertest.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import io.griesser.revolutdevelopertest.MainActivity;
import io.griesser.revolutdevelopertest.R;
import io.griesser.revolutdevelopertest.RevolutApplication;
import io.griesser.revolutdevelopertest.data.Currency;

/**
 * Created by rolan on 10/20/2017.
 */

public class CurrencyView implements View.OnFocusChangeListener, TextWatcher {

    private Currency currency;
    private View view;
    private TextView textViewCurrencyName;
    private TextView textViewRate;
    private EditText editTextAmount;
    private Double rate = 0d;
    private MainActivity mainActivity;
    private boolean hideRate;

    public CurrencyView(MainActivity mainActivity, Currency currency) {
        this.mainActivity = mainActivity;
        this.currency = currency;
        view = View.inflate(RevolutApplication.getAppContext(), R.layout.fragment_exchange_currency, null);
        textViewCurrencyName = view.findViewById(R.id.text_view_currency_name);
        textViewRate = view.findViewById(R.id.text_view_rate);
        editTextAmount = view.findViewById(R.id.edit_text_amount);

        hideRate = false;
        textViewCurrencyName.setText(currency.getIsoCode());
        editTextAmount.setHint("0");
        editTextAmount.setOnFocusChangeListener(this);
        editTextAmount.addTextChangedListener(this);
        textViewRate.setText("");
    }

    public Currency getCurrency(){
        return currency;
    }

    public View getView(){
        return view;
    }

    public void setFocused(){
        editTextAmount.requestFocus();
    }

    public boolean isFocused(){
        return editTextAmount.isFocused();
    }

    public void setRate(String currencySignTo, Double rate, boolean hideRate) {
        this.hideRate = hideRate;
        this.rate = rate;

        textViewRate.setText(String.format(RevolutApplication.getLocale(), view.getResources().getString(R.string.exchange_rate_format), this.currency.getSign(), currencySignTo, rate));
        setRateVisibility();
    }

    public Double getAmount() {

        String strInput = editTextAmount.getText().toString();

        try {
            return Double.parseDouble(strInput);
        }
        catch(NumberFormatException ex){ }
        return 0d;
    }

    public void setAmount(Double amount) {
        Double newAmount = amount * rate;
        if(newAmount != 0)
            editTextAmount.setText(String.format(RevolutApplication.getLocale(), "%.2f", newAmount));
        else
            editTextAmount.setText("");
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        setRateVisibility();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

    @Override
    public void afterTextChanged(Editable editable) { }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(isFocused()) {
            mainActivity.currencyViewTextChanged(this);
        }

        if ((charSequence != null) && (charSequence.length() > 5))
        {
            editTextAmount.setTextSize(34.0F);
            return;
        }
        editTextAmount.setTextSize(45.0F);
    }

    private void setRateVisibility() {
        textViewRate.setVisibility(hideRate || isFocused() ? View.INVISIBLE : View.VISIBLE);
    }
}
