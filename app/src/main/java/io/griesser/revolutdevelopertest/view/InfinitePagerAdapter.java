package io.griesser.revolutdevelopertest.view;

import android.database.DataSetObserver;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rolan on 10/20/2017.
 */

public class InfinitePagerAdapter extends PagerAdapter {

    private PagerAdapter innerAdapter;

    public InfinitePagerAdapter(PagerAdapter paramPagerAdapter)
    {
        innerAdapter = paramPagerAdapter;
    }

    public int getRealCount(){
        return innerAdapter.getCount();
    }

    public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
    {
        innerAdapter.destroyItem(paramViewGroup, paramInt % getRealCount(), paramObject);
    }

    public void finishUpdate(ViewGroup paramViewGroup)
    {
        innerAdapter.finishUpdate(paramViewGroup);
    }

    public int getCount()
    {
        if (getRealCount() == 0) {
            return 0;
        }
        return Integer.MAX_VALUE;
    }

    public int getItemPosition(Object paramObject)
    {
        return innerAdapter.getItemPosition(paramObject);
    }

    public CharSequence getPageTitle(int paramInt)
    {
        return innerAdapter.getPageTitle(paramInt % getRealCount());
    }

    public float getPageWidth(int paramInt)
    {
        return innerAdapter.getPageWidth(paramInt);
    }

    public Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
    {
        return innerAdapter.instantiateItem(paramViewGroup, paramInt % getRealCount());
    }

    public boolean isViewFromObject(View paramView, Object paramObject)
    {
        return innerAdapter.isViewFromObject(paramView, paramObject);
    }

    public void notifyDataSetChanged()
    {
        innerAdapter.notifyDataSetChanged();
    }

    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        innerAdapter.registerDataSetObserver(paramDataSetObserver);
    }

    public void restoreState(Parcelable paramParcelable, ClassLoader paramClassLoader)
    {
        innerAdapter.restoreState(paramParcelable, paramClassLoader);
    }

    public Parcelable saveState()
    {
        return innerAdapter.saveState();
    }

    public void setPrimaryItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
    {
        innerAdapter.setPrimaryItem(paramViewGroup, paramInt, paramObject);
    }

    public void startUpdate(ViewGroup paramViewGroup)
    {
        innerAdapter.startUpdate(paramViewGroup);
    }

    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        innerAdapter.unregisterDataSetObserver(paramDataSetObserver);
    }
}
