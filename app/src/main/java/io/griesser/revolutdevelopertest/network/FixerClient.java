package io.griesser.revolutdevelopertest.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.net.UnknownHostException;
import java.sql.Wrapper;
import java.util.concurrent.TimeUnit;

import io.griesser.revolutdevelopertest.data.ExchangeRate;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rolan on 10/21/2017.
 */

public class FixerClient {

    public static final String BASE_URL = "http://api.fixer.io/";
    private static final long DEFAULT_SUBSCRIBE_INTERVAL = 30 * 1000;

    private FixerApi fixerApi;

    public FixerClient() {

        Gson gson = new GsonBuilder()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        fixerApi = retrofit.create(FixerApi.class);
    }

    public ConnectableObservable<Wrapper<ExchangeRate>> createObservableOnGetExchangeRate(String base, String symbols) {
        return createObservableOnGetExchangeRate(base, symbols, DEFAULT_SUBSCRIBE_INTERVAL);
    }

    public ConnectableObservable<Wrapper<ExchangeRate>> createObservableOnGetExchangeRate(String base, String symbols, final long interval) {

        ConnectableObservable<Wrapper<ExchangeRate>> dataObservable =
                fixerApi.getExchangeRate(base, symbols)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<ExchangeRate, Wrapper<ExchangeRate>>() {
                    @Override
                    public Wrapper<ExchangeRate> apply(@NonNull ExchangeRate exchangeRate) throws Exception {
                        return Wrapper.response(exchangeRate);
                    }
                })
                // Retry a few times before letting an error through.
                .retry(5)
                .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends Wrapper<ExchangeRate>>>() {
                    @Override
                    public ObservableSource<? extends Wrapper<ExchangeRate>> apply(@NonNull Throwable throwable) throws Exception {
                        //// If the server is unreachable, let the subscriber know.
                        //// The user may be able to do something about his connection.
                        if (throwable instanceof UnknownHostException) {
                            // Recoverable error - transmit error without breaking.
                            return Observable.just(Wrapper.<ExchangeRate>error(throwable));
                        }
                        return Observable.error(throwable);
                    }
                })
                .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(@NonNull Observable<Object> objectObservable) throws Exception {
                        return Observable.interval(interval, TimeUnit.MILLISECONDS);
                    }
                })
                .replay(1);

        dataObservable.connect();
        return dataObservable;
    }


    public static class Wrapper<ResponseType> {
        public Throwable error;
        public ResponseType response;

        private Wrapper() { }

        private static <T> Wrapper<T> error(Throwable t) {
            Wrapper<T> wrapper = new Wrapper<>();
            wrapper.error = t;
            return wrapper;
        }

        private static <T> Wrapper<T> response(T response) {
            Wrapper<T> wrapper = new Wrapper<>();
            wrapper.response = response;
            return wrapper;
        }

        public boolean hasError() { return error != null; }
    }
}
