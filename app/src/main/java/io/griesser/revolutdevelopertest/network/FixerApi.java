package io.griesser.revolutdevelopertest.network;

import io.griesser.revolutdevelopertest.data.ExchangeRate;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rolan on 10/20/2017.
 */

public interface FixerApi {
    @GET("latest")
    Observable<ExchangeRate> getExchangeRate(@Query("base") String base, @Query("symbols") String symbols);
}
