package io.griesser.revolutdevelopertest;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import java.util.Locale;

/**
 * Created by rolan on 10/20/2017.
 */

public class RevolutApplication extends Application {

    private static Context context;
    private static Locale locale;

    public void onCreate() {
        super.onCreate();
        RevolutApplication.context = getApplicationContext();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            RevolutApplication.locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            RevolutApplication.locale = context.getResources().getConfiguration().locale;
        }
    }

    public static Context getAppContext() {
        return RevolutApplication.context;
    }

    public static Locale getLocale(){
        return RevolutApplication.locale;
    }
}
