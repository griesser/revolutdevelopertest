package io.griesser.revolutdevelopertest.data;

/**
 * Created by rolan on 10/21/2017.
 */

public class Currency {
    private String isoCode;
    private String sign;

    public Currency(String isoCode, String sign) {
        this.isoCode = isoCode;
        this.sign = sign;
    }

    @Override
    public String toString() {
        return isoCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
