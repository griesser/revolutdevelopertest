package io.griesser.revolutdevelopertest;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.rd.PageIndicatorView;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.griesser.revolutdevelopertest.data.Currency;
import io.griesser.revolutdevelopertest.data.ExchangeRate;
import io.griesser.revolutdevelopertest.dialog.ErrorDialogFragment;
import io.griesser.revolutdevelopertest.network.FixerApi;
import io.griesser.revolutdevelopertest.network.FixerClient;
import io.griesser.revolutdevelopertest.view.CurrencyView;
import io.griesser.revolutdevelopertest.view.InfinitePagerAdapter;
import io.griesser.revolutdevelopertest.view.InfiniteViewPager;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

    private List<Currency> availableCurrencies = new ArrayList<>(Arrays.asList(
            new Currency("EUR", "€"),
            new Currency("USD", "$"),
            new Currency("GBP", "£")));

    private Map<Currency, Double> exchangeRates = new HashMap<>();

    private InfiniteViewPager pagerFrom;
    private InfiniteViewPager pagerTo;
    private ExchangePagerAdapter pagerAdapterFrom;
    private ExchangePagerAdapter pagerAdapterTo;
    private CurrencyView currentCurrencyViewFrom;
    private CurrencyView currentCurrencyViewTo;
    private CompositeDisposable disposables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        disposables = new CompositeDisposable();

        PageIndicatorView pageIndicatorFrom = (PageIndicatorView)findViewById(R.id.page_indicator_from);
        PageIndicatorView pageIndicatorTo = (PageIndicatorView)findViewById(R.id.page_indicator_to);

        pagerAdapterFrom = new ExchangePagerAdapter(availableCurrencies);
        pagerFrom = (InfiniteViewPager)findViewById(R.id.pager_from);
        pagerFrom.setAdapter(new InfinitePagerAdapter(pagerAdapterFrom));
        pagerFrom.addOnPageChangeListener(new OnViewPagerPageChangeListener(0, pageIndicatorFrom, pagerAdapterFrom.getCount()));

        pagerAdapterTo = new ExchangePagerAdapter(availableCurrencies);
        pagerTo = (InfiniteViewPager)findViewById(R.id.pager_to);
        pagerTo.setAdapter(new InfinitePagerAdapter(pagerAdapterTo));
        pagerTo.addOnPageChangeListener(new OnViewPagerPageChangeListener(1, pageIndicatorTo, pagerAdapterTo.getCount()));

        pageIndicatorFrom.setCount(pagerAdapterFrom.getCount());
        pageIndicatorTo.setCount(pagerAdapterTo.getCount());

        if(pagerAdapterFrom.getCount() > 0)
        {
            pagerFrom.setCurrentItem(0);
            currentCurrencyViewFrom = pagerAdapterFrom.get(pagerFrom.getCurrentItem());
        }
        if(pagerAdapterTo.getCount() > 0) {
            pagerTo.setCurrentItem(1 % pagerAdapterTo.getCount());
            currentCurrencyViewTo = pagerAdapterTo.get(pagerTo.getCurrentItem());
        }

        if(availableCurrencies.size() > 0) {
            String symbols = TextUtils.join(",", availableCurrencies.subList(1, availableCurrencies.size()));
            FixerClient fixerClient = new FixerClient();
            disposables.add(fixerClient.createObservableOnGetExchangeRate(availableCurrencies.get(0).getIsoCode(), symbols)
                    .subscribe(new GetExchangeRateConsumer(), new ErrorConsumer()));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }

    public void currencyViewTextChanged(CurrencyView currencyViewFrom) {
        CurrencyView currencyViewTo = currentCurrencyViewFrom;
        if(currencyViewTo == currencyViewFrom)
            currencyViewTo = currentCurrencyViewTo;

        updateCurrencyViews(currencyViewFrom, currencyViewTo);
    }

    private void updateCurrencyViews(CurrencyView viewFrom, CurrencyView viewTo){

        if(viewFrom.getCurrency() == viewTo.getCurrency())
            viewTo.setAmount(0d);
        else
            viewTo.setAmount(viewFrom.getAmount());
    }

    private void showErrorDialog(String message) {
        FragmentManager fm = getSupportFragmentManager();
        ErrorDialogFragment alertDialog = ErrorDialogFragment.newInstance(message);
        alertDialog.show(fm, "fragment_alert");
    }

    private void updateCurrencyViews() {
        if(currentCurrencyViewFrom == null ||
                currentCurrencyViewTo == null ||
                !exchangeRates.containsKey(currentCurrencyViewFrom.getCurrency()) ||
                !exchangeRates.containsKey(currentCurrencyViewTo.getCurrency()))
            return;

        Double rate1 = exchangeRates.get(currentCurrencyViewFrom.getCurrency());
        Double rate2 = exchangeRates.get(currentCurrencyViewTo.getCurrency());

        boolean sameCurrency = currentCurrencyViewFrom.getCurrency() == currentCurrencyViewTo.getCurrency();

        Double multiplier = rate2 / rate1;
        currentCurrencyViewFrom.setRate(currentCurrencyViewTo.getCurrency().getSign(), 1 / multiplier, sameCurrency);
        currentCurrencyViewTo.setRate(currentCurrencyViewFrom.getCurrency().getSign(), multiplier, sameCurrency);

        CurrencyView focusedView = currentCurrencyViewFrom.isFocused() ? currentCurrencyViewFrom : currentCurrencyViewTo;
        CurrencyView viewToUpdate = focusedView == currentCurrencyViewTo ? currentCurrencyViewFrom : currentCurrencyViewTo;

        updateCurrencyViews(focusedView, viewToUpdate);
    }

    private class ExchangePagerAdapter extends PagerAdapter {

        private List<CurrencyView> _currencies;

        private ExchangePagerAdapter(List<Currency> currencies) {
            _currencies = new ArrayList<>();

            for (int i = 0; i < currencies.size(); i++) {
                _currencies.add(new CurrencyView(MainActivity.this, currencies.get(i)));
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

        }

        private CurrencyView get(int index) {
            return _currencies.get(index);
        }

        public int getCount() {
            return _currencies.size();
        }

        public Object instantiateItem(ViewGroup paramViewGroup, int i)
        {
            View localView = _currencies.get(i).getView();
            if (localView == null) {
                return null;
            }
            localView.setTag(_currencies.get(i));
            ViewParent localViewParent = localView.getParent();
            if ((localViewParent != null) && ((localViewParent instanceof ViewGroup))) {
                ((ViewGroup)localViewParent).removeView(localView);
            }
            paramViewGroup.addView(localView);
            return _currencies.get(i);
        }

        public boolean isViewFromObject(View paramView, Object paramObject)
        {
            return paramView.getTag() == paramObject;
        }
    }


    private class OnViewPagerPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        private PageIndicatorView _indicator;
        private int _realCount;
        private int _pager;

        private OnViewPagerPageChangeListener(int pager, PageIndicatorView indicator, int realCount) {
            _pager = pager;
            _indicator = indicator;
            _realCount = realCount;
        }

        @Override
        public void onPageSelected(int position) {
            position %= _realCount;

            super.onPageSelected(position);

            if(currentCurrencyViewTo != null && currentCurrencyViewFrom != null) {
                boolean isFirstFocused = currentCurrencyViewFrom.isFocused();

                currentCurrencyViewFrom = pagerAdapterFrom.get(pagerFrom.getCurrentItem());
                currentCurrencyViewTo = pagerAdapterTo.get(pagerTo.getCurrentItem());

                if (isFirstFocused) {
                    if (_pager == 0)
                        currentCurrencyViewFrom.setAmount(0d);
                    currentCurrencyViewFrom.setFocused();
                } else {
                    if (_pager == 1)
                        currentCurrencyViewTo.setAmount(0d);
                    currentCurrencyViewTo.setFocused();
                }

                MainActivity.this.updateCurrencyViews();
            }

            _indicator.setSelection(position);
        }
    }

    private class GetExchangeRateConsumer implements Consumer<FixerClient.Wrapper<ExchangeRate>> {
        @Override
        public void accept(FixerClient.Wrapper<ExchangeRate> wrapper) throws Exception {
            if (wrapper.hasError()) {
                if(wrapper.error instanceof UnknownHostException) {
                    showErrorDialog(String.format(RevolutApplication.getLocale(), getString(R.string.error_api_not_reachable), FixerClient.BASE_URL));
                    disposables.clear();
                }
            } else {

                for (Currency currency : availableCurrencies) {
                    if (currency.getIsoCode().equals(wrapper.response.getBase()))
                        exchangeRates.put(currency, 1d);
                    else
                        exchangeRates.put(currency, wrapper.response.getRates().containsKey(currency.getIsoCode())
                                ? wrapper.response.getRates().get(currency.getIsoCode())
                                : -1);
                }

                updateCurrencyViews();
            }
        }
    }

    private class ErrorConsumer implements Consumer<Throwable> {
        @Override
        public void accept(Throwable throwable) throws Exception {
            String message = "";
            if(throwable instanceof HttpException) {
                if(((HttpException)throwable).code() == 404) {
                    message += getString(R.string.error_api_path_not_found);
                }
            }
            else {
                message += getString(R.string.error_api_unknown);
            }
            message = String.format(RevolutApplication.getLocale(), message, FixerClient.BASE_URL) + throwable.getLocalizedMessage();
            showErrorDialog(message);
            disposables.clear();
        }
    }
}
